const http = require('http')
const express = require('express');
const app = express()
const port = 502
const misRutas = require("./router/index")
const path = require("path");
const{renderFile} =require('ejs')
app.set('view engine', 'ejs');
app.engine('html',require('ejs').renderFile)
app.use(express.static(__dirname + '/public'))
const bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({extended:true}))
app.use(misRutas);

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');

})

app.use(express.static(__dirname + '/public' ))

app.listen(port,()=>{
    console.log('Inciado el puerto 502')
})
